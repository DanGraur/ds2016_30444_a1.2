package logic.exception;

/**
 * Created by noi on 10/14/2016.
 */
public class IncompleteObjectException extends Exception {

    public IncompleteObjectException() {
        super("The supplied object is not complete");
    }

    public IncompleteObjectException(String message) {
        super(message);
    }
}
