package logic.exception;

public class WebServiceException extends Exception {

    public WebServiceException() {
        super("There was an error when processing the WebService response XML");
    }

    public WebServiceException(String message) {
        super(message);
    }
}
