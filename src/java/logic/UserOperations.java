package logic;

import data.beans.City;
import data.beans.Flight;
import data.city.CityDAO;
import data.city.CityDAOImpl;
import data.flight.FlightDAO;
import data.flight.FlightDAOImpl;
import data.session_manager.SessionManager;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import logic.exception.WebServiceException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.*;

public class UserOperations {
    /**
     * Google Timezone API key
     */
    private final String TIMEZONE_API_KEY = "AIzaSyD1hN5QQ3_rqijPbgYt7oHSyI5XGntmOp4";

    FlightDAO flightDAO;
    CityDAO cityDAO;

    public UserOperations() {
        flightDAO = new FlightDAOImpl(SessionManager.getSessionFactory());
        cityDAO = new CityDAOImpl(SessionManager.getSessionFactory());
    }

    /**
     * Get all the flights in the DB
     *
     * @return all the flights in the DB
     */
    public List<Flight> findAllFlights() {
        return flightDAO.findAllFlights();
    }

    /**
     * Get all the cities in the DB
     *
     * @return all the cities in the DB
     */
    public List<City> findAllCities() {
        return cityDAO.findAllCities();
    }

    /**
     * Generates a map which maps the unique key of the city in the DB, to the city object
     *
     * @return returns a map between city identifiers, and city objects
     */
    public Map<Integer, City> getAllCitiesMap() {
        Iterator<City> iter = cityDAO.findAllCities().iterator();
        Map<Integer, City> citiesMap = new HashMap<Integer, City>();

        while(iter.hasNext()) {
            City city = iter.next();

            citiesMap.put(city.getId(), city);
        }

        return citiesMap;
    }


    // THE TIME RETRIEVED IS NOT UTC IT IS THE LOCAL-TIME ---> NEEDS FIXING
    /**
     * Retrieves the local time of a city using a web service located at URL: 'https://maps.googleapis.com/maps/api/timezone'
     *
     * @param city the city, containing latitude, and longitude information
     * @return the local time of the city in a String format
     */
    private long getTimeFromWebService(City city) throws WebServiceException {
        // Get the UTC time
        DateTime utcTime = DateTime.now(DateTimeZone.UTC);
        Document webXML = null;

        // Get the response from the server
        String urlOfTimezoneWebService = "https://maps.googleapis.com/maps/api/timezone/xml?" +
                "location=" + city.getLatitude() +  ',' + city.getLongitude() + '&' +
                "timestamp=" + (utcTime.getMillis() / 1000) + '&' +
                "key=" + TIMEZONE_API_KEY;

        //System.out.println(urlOfTimezoneWebService);

        // Build an XML document by reading it from the url of the web service request
        try {
            // Establish a connection
            URL url = new URL(urlOfTimezoneWebService);
            URLConnection connection = url.openConnection();

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            // Build the XML document in memory
            webXML = builder.parse(connection.getInputStream());

        } catch (IOException | ParserConfigurationException | SAXException e) {
            throw new WebServiceException();
        }

        // Retrieve the actual information from the XML
        NodeList rawOffset = webXML.getElementsByTagName("raw_offset");
        NodeList dstOffset = webXML.getElementsByTagName("dst_offset");

        // Make sure that there is such a node, else throw an exception
        if (rawOffset.item(0) == null || dstOffset.item(0) == null)
            throw new WebServiceException("Could not properly parse the server response");

        // Return the local time of the city
        return  utcTime.getMillis() + //((long) Double.parseDouble(rawOffset.item(0).getTextContent())) * 1000;
               ((long) Double.parseDouble(rawOffset.item(0).getTextContent()) +
                (long) Double.parseDouble(dstOffset.item(0).getTextContent())) *
                1000;
    }

    // THE TIME RETRIEVED IS NOT UTC IT IS THE LOCAL-TIME ---> NEEDS FIXING
    /**
     * Retrieves the local time of a city using a web service located at URL: 'http://www.new.earthtools.org/timezone/'
     *
     * @param city the city, containing latitude, and longitude information
     * @return the local time of the city in a String format
     */
    private String getTimeFromWebServiceEarthTools(City city) throws WebServiceException {
        // Get the UTC time
        DateTime utcTime = DateTime.now(DateTimeZone.UTC);
        Document webXML = null;

        // Get the response from the server
        String urlOfTimezoneWebService = "http://www.new.earthtools.org/timezone/" + city.getLatitude() + '/' + city.getLongitude();

        //System.out.println(urlOfTimezoneWebService);

        // Build an XML document by reading it from the url of the web service request
        try {
            // Establish a connection
            URL url = new URL(urlOfTimezoneWebService);
            URLConnection connection = url.openConnection();

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            // Build the XML document in memory
            webXML = builder.parse(connection.getInputStream());

        } catch (IOException | ParserConfigurationException | SAXException e) {
            throw new WebServiceException();
        }

        // Retrieve the actual information from the XML
        NodeList localtime = webXML.getElementsByTagName("localtime");

        // Make sure that there is such a node, else throw an exception
        if (localtime.item(0) == null)
            throw new WebServiceException("Could not properly parse the server response");

        // Return the local time of the city
        return localtime.item(0).getTextContent();
    }

    /**
     * WIll return the local time of a city, found in the DB, using an external web service
     *
     * @param id the id of the city whose local time is being retrieved
     * @return a String with the local time of the city, or an error message
     */
    public String getCityLocalTime(int id) {
        City city = cityDAO.findCity(id);
        Date date = new Date();

        // If such a city exists (this should always be true
        if (city != null)
            try {
                /*
                // Using the Google Timezone API
                // Set the date
                date.setTime(getTimeFromWebService(city));

                // Return a formatted string of the time
                return DateFormat.getTimeInstance().format(date);
                */
                // Using the EarthTools API
                return getTimeFromWebServiceEarthTools(city);
            } catch (WebServiceException e) {
                System.out.println(e.getMessage());

                e.printStackTrace();
            }
        else
            return "No such city could be found";

        return "";
    }

}
