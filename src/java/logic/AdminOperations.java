package logic;

import data.beans.City;
import data.beans.Flight;
import data.beans.FormattedDatePair;
import data.city.CityDAO;
import data.city.CityDAOImpl;
import data.flight.FlightDAO;
import data.flight.FlightDAOImpl;
import data.object_checker.ObjectChecker;
import data.session_manager.SessionManager;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AdminOperations {
    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public AdminOperations() {
        flightDAO = new FlightDAOImpl(SessionManager.getSessionFactory());
        cityDAO = new CityDAOImpl(SessionManager.getSessionFactory());
    }

    /**
     * Adds a new flight to the DB
     *
     * @param flight the new flight object to be added
     * @return a string with a success, or failure message
     */
    public String addFlight(Flight flight) {

        // Check that the flight object values for all the fields
        try {
            if (!ObjectChecker.isConsistent(flight))
                return "ERROR: Object is inconsistent";
        } catch (IllegalAccessException e) {
            return "ERROR: Could not properly check the object's consistency";
        }

        if (flight.getArrivalDate().getTime() < flight.getDepartureDate().getTime())
            return "WARNING: Arrival date before departure date";

        flightDAO.addFlight(flight);

        return "Flight added successfully";
    }

    /**
     * Updates an existing flight in the DB
     *
     * @param flight the flight object to be updated
     * @return a string with a success, or failure message
     */
    public String updateFlight(Flight flight) {

        // Check that the flight object values for all the fields
        try {
            if (!ObjectChecker.isConsistent(flight))
                return "ERROR: Object is inconsistent";
        } catch (IllegalAccessException e) {
            return "ERROR: Could not properly check the object's consistency";
        }

        if (flight.getArrivalDate().getTime() < flight.getDepartureDate().getTime()) {
            flightDAO.updateFlight(flight);

            return "WARNING: Arrival date before departure date";
        }

        flightDAO.updateFlight(flight);

        return "Flight updated successfully";
    }


    /**
     * Deletes a flight from the DB
     *
     * @param id the id of the flight to be deleted
     */
    public void deleteFlight(int id) {
        flightDAO.deleteFlight(id);
    }

    /**
     * Finds a flight in the DB
     *
     * @param id the id of the flight to be fetched
     * @return the flight object, if found, null otherwise
     */
    public Flight findFlight(int id) {
        return flightDAO.findFlight(id);
    }

    /**
     * Returns a list with all the flights in the DB
     *
     * @return a list with all the flights in the DB
     */
    public List<Flight> findAllFlights() {
        return flightDAO.findAllFlights();
    }

    /**
     * Get all the cities in the DB
     *
     * @return all the cities in the DB
     */
    public List<City> findAllCities() {
        return cityDAO.findAllCities();
    }

    /**
     * Generates a map which maps the unique key of the city in the DB, to the city object
     *
     * @return returns a map between city identifiers, and city objects
     */
    public Map<Integer, City> getAllCitiesMap() {
        Iterator<City> iter = cityDAO.findAllCities().iterator();
        Map<Integer, City> citiesMap = new HashMap<Integer, City>();

        while(iter.hasNext()) {
            City city = iter.next();

            citiesMap.put(city.getId(), city);
        }

        return citiesMap;
    }

    public Map<Integer, FormattedDatePair> getAllDatePairs() {
        Iterator<Flight> iter = flightDAO.findAllFlights().iterator();
        Map<Integer, FormattedDatePair> pairMap = new HashMap<Integer, FormattedDatePair>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        while(iter.hasNext()) {
            Flight flight = iter.next();

            pairMap.put(flight.getId(), new FormattedDatePair(format.format(flight.getDepartureDate()), format.format(flight.getArrivalDate())));
        }

        return pairMap;
    }
}
