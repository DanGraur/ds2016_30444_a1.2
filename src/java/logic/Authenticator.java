package logic;

import data.beans.User;
import data.session_manager.SessionManager;
import data.user.UserDAO;
import data.user.UserDAOImpl;
import org.hibernate.cfg.Configuration;

/**
 * Created by noi on 10/14/2016.
 */
public class Authenticator {
    private UserDAO userDAO;

    public Authenticator() {
        userDAO = new UserDAOImpl(SessionManager.getSessionFactory());
    }

    /**
     * Returns the found user, or null if no user has been found with the (username, password) pair
     *
     * @param userName the username
     * @param password the password
     * @return the user object if the login was a success, null otherwise
     */
    public User authenticate(String userName, String password) {



        User user = userDAO.findUser(userName, password);

        if (user != null)
            // Check if it matches when the case sensitivity is taken into account
            if (user.getPassword().equals(password))
                // Protect the password
                user.setPassword("");
            else
                // GC the object, and implicitly invalidate the login
                user = null;

        return user;

    }

}
