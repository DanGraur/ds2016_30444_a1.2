package data.flight;

import data.beans.Flight;

import java.util.List;

/**
 * Created by noi on 10/14/2016.
 */
public interface FlightDAO {
    int addFlight(Flight flight);

    void updateFlight(Flight flight);

    void deleteFlight(int id);

    Flight findFlight(int id);

    List<Flight> findAllFlights();
}
