package data.flight;


import data.beans.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;

public class FlightDAOImpl implements FlightDAO {

    private SessionFactory factory;

    public FlightDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    public int addFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            // Request insertion of new flight
            session.save(flight);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return flight.getId();
    }

    public void updateFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            // Request update of new flight
            session.update(flight);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    public void deleteFlight(int id) {
        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            // Request addition of new flight
            Query deleteQuery = session.createQuery("DELETE FROM Flight WHERE fid = :id");
            deleteQuery.setParameter("id", id);

            deleteQuery.executeUpdate();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
    }

    public Flight findFlight(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight flight = null;

        try {
            tx = session.beginTransaction();

            Query searchQuery = session.createQuery("FROM Flight WHERE fid = :id");

            searchQuery.setParameter("id", id);

            flight = (Flight) searchQuery.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return flight;
    }

    public List<Flight> findAllFlights() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> list = null;

        try {
            tx = session.beginTransaction();

            list = session.createQuery("FROM Flight").list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return list;
    }
}
