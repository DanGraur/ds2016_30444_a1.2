package data.object_checker;

import logic.exception.IncompleteObjectException;

import java.lang.reflect.Field;

/**
 * Class used for checking various object properties, such as consistency
 */
public class ObjectChecker {

    /**
     * Checks to see if the object's fields have values (all of them)
     * @param object the object to be checked
     * @return true if the object's fields all have values, false otherwise
     * @throws IncompleteObjectException
     */
    public static boolean isConsistent(Object object) throws IllegalAccessException {

        if (object == null)
            return false;

        Class objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();

        boolean consistent = true;

        for (Field f : fields) {
            boolean wasAccessible = f.isAccessible();
            f.setAccessible(true);

            if (f.get(object) == null ||
               (f.getType().equals(String.class) && ((String) f.get(object)).equals("")))
                consistent = false;

            f.setAccessible(wasAccessible);

            if (!consistent)
                return false;
        }


        return true;
    }
}
