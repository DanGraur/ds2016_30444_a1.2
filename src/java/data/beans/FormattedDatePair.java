package data.beans;

/**
 * Created by noi on 10/15/2016.
 */
public class FormattedDatePair {
    private String arrivalDate;
    private String departureDate;

    public FormattedDatePair(String departureDate, String arrivalDate) {
        this.arrivalDate = arrivalDate;
        this.departureDate = departureDate;
    }

    @Override
    public String toString() {
        return "FormattedDatePair{" +
                "arrivalDate='" + arrivalDate + '\'' +
                ", departureDate='" + departureDate + '\'' +
                '}';
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }
}
