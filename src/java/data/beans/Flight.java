package data.beans;

import java.io.Serializable;
import java.sql.Date;

public class Flight implements Serializable {
    private int id;
    private String airplaneType;
    private int departureCid;
    private Date departureDate;
    private int departureHour;
    private int arrivalCid;
    private Date arrivalDate;
    private int arrivalHour;

    public Flight() {
    }

    public Flight(int id, String airplaneType, int departureCid, Date departureDate, int departureHour, int arrivalCid, Date arrivalDate, int arrivalHour) {
        this.id = id;
        this.airplaneType = airplaneType;
        this.departureCid = departureCid;
        this.departureDate = departureDate;
        this.departureHour = departureHour;
        this.arrivalCid = arrivalCid;
        this.arrivalDate = arrivalDate;
        this.arrivalHour = arrivalHour;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCid=" + departureCid +
                ", departureDate=" + departureDate +
                ", departureHour=" + departureHour +
                ", arrivalCid=" + arrivalCid +
                ", arrivalDate=" + arrivalDate +
                ", arrivalHour=" + arrivalHour +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public int getDepartureCid() {
        return departureCid;
    }

    public void setDepartureCid(int departureCid) {
        this.departureCid = departureCid;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public int getDepartureHour() {
        return departureHour;
    }

    public void setDepartureHour(int departureHour) {
        this.departureHour = departureHour;
    }

    public int getArrivalCid() {
        return arrivalCid;
    }

    public void setArrivalCid(int arrivalCid) {
        this.arrivalCid = arrivalCid;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(int arrivalHour) {
        this.arrivalHour = arrivalHour;
    }
}
