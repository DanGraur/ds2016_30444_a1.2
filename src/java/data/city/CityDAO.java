package data.city;

import data.beans.City;

import java.util.List;

/**
 * Created by noi on 10/14/2016.
 */
public interface CityDAO {
    City findCity(int id);

    List<City> findAllCities();
}
