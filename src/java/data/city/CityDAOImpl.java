package data.city;

import data.beans.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by noi on 10/14/2016.
 */
public class CityDAOImpl implements CityDAO {

    private SessionFactory factory;

    public CityDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    public City findCity(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        City city = null;

        try {
            tx = session.beginTransaction();

            Query searchQuery = session.createQuery("FROM City WHERE cid = :id");

            searchQuery.setParameter("id", id);

            city = (City) searchQuery.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return city;
    }

    public List<City> findAllCities() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> list = null;

        try {
            tx = session.beginTransaction();

            list = session.createQuery("FROM City").list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return list;
    }

}
