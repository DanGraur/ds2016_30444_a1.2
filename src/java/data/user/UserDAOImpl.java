package data.user;

import data.beans.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

/**
 * Created by noi on 10/14/2016.
 */
public class UserDAOImpl implements UserDAO {

    private SessionFactory factory;

    public UserDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    public User findUser(String userName, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;

        try {
            tx = session.beginTransaction();

            Query searchQuery = session.createQuery("FROM User WHERE name = :name AND password = :password");

            searchQuery.setParameter("name", userName);
            searchQuery.setParameter("password", password);

            user = (User) searchQuery.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }

        return user;
    }
}
