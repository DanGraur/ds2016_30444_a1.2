package data.user;

import data.beans.User;

/**
 * Created by noi on 10/14/2016.
 */
public interface UserDAO {
    User findUser(String userName, String password);
}
