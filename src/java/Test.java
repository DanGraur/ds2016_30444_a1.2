import data.beans.City;
import data.beans.User;
import data.city.CityDAO;
import data.city.CityDAOImpl;
import data.flight.FlightDAO;
import data.flight.FlightDAOImpl;
import data.beans.Flight;
import data.object_checker.ObjectChecker;
import data.session_manager.SessionManager;
import data.user.UserDAO;
import data.user.UserDAOImpl;
import logic.Authenticator;
import logic.UserOperations;
import logic.exception.IncompleteObjectException;

/**
 * Created by noi on 10/14/2016.
 */

public class Test {

    private void testDB() {
        UserDAO userDAO = new UserDAOImpl(SessionManager.getSessionFactory());
        CityDAO cityDAO = new CityDAOImpl(SessionManager.getSessionFactory());
        FlightDAO flightDAO = new FlightDAOImpl(SessionManager.getSessionFactory());

        //System.out.println(userDAO.findUser("admin", "password"));
        //System.out.println(cityDAO.findCity(1));
        //System.out.println(flightDAO.findFlight(1));

        // Test City find all
        for (City c : cityDAO.findAllCities())
            System.out.println(c);

        /*
        // Test update Flights
        Flight flight = flightDAO.findFlight(2);

        flight.setArrivalDate(new Date(1476451041L));
        flight.setDepartureCid(2);
        flight.setAirplaneType("Airbus");

        flightDAO.updateFlight(flight);
        */

        /*
        // Test Delete Flight
        flightDAO.deleteFlight(2);
        */

        // Test Add Flight
        //System.out.println(flightDAO.addFlight(new Flight(0, "Dacia", 2, new Date(1476451679L), 9, 1, new Date(1476451678L), 12)));


        // Find all cities
        for (Flight f : flightDAO.findAllFlights())
            System.out.println(f);
    }

    public static void main(String[] args) {
       //System.out.println((new UserOperations()).getCityLocalTime(3));


    }
}
