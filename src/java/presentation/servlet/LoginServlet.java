package presentation.servlet;


import data.beans.User;
import logic.AdminOperations;
import logic.Authenticator;
import logic.UserOperations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    /**
     * Will handle the initial page load-up
     *
     * @param request the client request
     * @param response the server response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/pages/login.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Will handle the login operation
     *
     * @param request the client request
     * @param response the server response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Creates a session if the user doesn't already have one
        // Will add a Cookie by default with a 30 min expiration time
        HttpSession userSession = request.getSession();

        RequestDispatcher dispatcher;

        Authenticator auth = new Authenticator();
        User user = auth.authenticate(request.getParameter("username"), request.getParameter("pass"));

        if (user != null) {
            // Set the user object to the session
            userSession.setAttribute("user", user);

            // Add a corresponding operation object to the session to do the work
            userSession.setAttribute("operation", (user.isAdmin()) ? (new AdminOperations()) : (new UserOperations()));

            // Dispatch to the corresponding page, where credentials will be checked once more
            if (user.isAdmin())
                response.sendRedirect("/airport/admin");
            else
                response.sendRedirect("/airport/user");

        } else {
            request.setAttribute("errorMessage", "Invalid Username or Password provided");
            request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
        }

    }
}
