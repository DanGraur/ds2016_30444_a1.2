package presentation.servlet;

import data.beans.Flight;
import data.beans.User;
import logic.AdminOperations;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AdminServlet extends HttpServlet {
    private AdminOperations operationGateway;

    @Override
    public void init() throws ServletException {
        operationGateway = new AdminOperations();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get the session
        HttpSession userSession = request.getSession();

        // Pass them to the JSP
        request.setAttribute("flights", operationGateway.findAllFlights());
        request.setAttribute("cities", operationGateway.getAllCitiesMap());
        request.setAttribute("datePairs", operationGateway.getAllDatePairs());
        request.getRequestDispatcher("/WEB-INF/pages/admin.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession userSession = request.getSession();

        // Get the user from the session
        User user = (User) userSession.getAttribute("user");

        String statusReport = "";
        String buttonName = request.getParameter("button");

        if (buttonName.equals("Add Flight")) {
            // Get the dates from the form
            long departureDateMillis = 0L;
            long arrivalDateMillis = 0L;
            boolean correctDates = true;

            try {
                Date departureDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(request.getParameter("newFlightDepDate"));
                Date arrivalDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(request.getParameter("newFlightArrDate"));

                departureDateMillis = departureDate.getTime();
                arrivalDateMillis = arrivalDate.getTime();

            } catch (Exception e) {
                correctDates = false;
                statusReport = "One of the dates was not properly configured";
            }

            // Create a new flight object, and add it to the DB
            if (correctDates)
                statusReport = operationGateway.addFlight(new Flight(0,
                        request.getParameter("newFlightAirplaneType"),
                        Integer.parseInt(request.getParameter("newFlightDepCid")),
                        new java.sql.Date(departureDateMillis),
                        Integer.parseInt(request.getParameter("newFlightDepHour")),
                        Integer.parseInt(request.getParameter("newFlightArrCid")),
                        new java.sql.Date(arrivalDateMillis),
                        Integer.parseInt(request.getParameter("newFlightArrHour"))));

            // Display the new Flights DB
            request.setAttribute("statusMessage", statusReport);

        } else if (buttonName.equals("Delete Flight")) {
            int id = Integer.parseInt(request.getParameter("hiddenId"));

            operationGateway.deleteFlight(id);
        } else if (buttonName.equals("Update Flight")) {
            String statusMessage;
            Flight flight = null;

            try {
                flight = new Flight(Integer.parseInt(request.getParameter("hiddenId")),
                        request.getParameter("hiddenAirplaneType"),
                        Integer.parseInt(request.getParameter("hiddenDepCid")),
                        new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("hiddenDepDate")).getTime()),
                        Integer.parseInt(request.getParameter("hiddenDepHour")),
                        Integer.parseInt(request.getParameter("hiddenArrCid")),
                        new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("hiddenArrDate")).getTime()),
                        Integer.parseInt(request.getParameter("hiddenArrHour")));

            } catch (ParseException e) {
                statusMessage = "One of the dates was not properly configured";
            }

            statusMessage = operationGateway.updateFlight(flight);
            request.setAttribute("statusMessage", statusMessage);
        } else {
            // Can only be log out button
            userSession.invalidate();
            response.sendRedirect("/airport/login");
            return;
        }

        // Common to all cases
        request.setAttribute("flights", operationGateway.findAllFlights());
        request.setAttribute("cities", operationGateway.getAllCitiesMap());
        request.setAttribute("datePairs", operationGateway.getAllDatePairs());
        request.getRequestDispatcher("/WEB-INF/pages/admin.jsp").forward(request, response);

    }
}
