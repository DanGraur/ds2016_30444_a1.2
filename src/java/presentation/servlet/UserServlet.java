package presentation.servlet;

import data.beans.City;
import data.beans.Flight;
import data.beans.User;
import logic.UserOperations;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public class UserServlet extends HttpServlet {
    private UserOperations operationGateway;

    @Override
    public void init() throws ServletException {
        operationGateway = new UserOperations();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get the session
        HttpSession userSession = request.getSession();

        // Get the user from the session
        User user = (User) userSession.getAttribute("user");

        // Get the resources from the DB
        List<Flight> flights = operationGateway.findAllFlights();
        Map<Integer, City> cityMap = operationGateway.getAllCitiesMap();

        // Store them in the session
        userSession.setAttribute("flights", flights);
        userSession.setAttribute("cities", cityMap);

        // Pass them to the JSP
        request.setAttribute("flights", flights);
        request.setAttribute("cities", cityMap);
        request.getRequestDispatcher("/WEB-INF/pages/user.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession userSession = request.getSession();

        // Get the user from the session
        User user = (User) userSession.getAttribute("user");

        // Check for logout request
        if (request.getParameter("logOutButton") != null) {
            userSession.invalidate();
            response.sendRedirect("/airport/login");
            return;
        }

        // If no logout request, then we have a local time request
        int id = Integer.parseInt(request.getParameter("hiddenId"));

        // Get the local time
        String localTime = operationGateway.getCityLocalTime(id);
        Map<Integer, City> cityMap = (Map<Integer, City>) userSession.getAttribute("cities");

        // Set the page attributes
        request.setAttribute("localTime", localTime);
        request.setAttribute("localTimeCity", cityMap.get(id).getName());
        request.setAttribute("flights", userSession.getAttribute("flights")); // OPTIONALLY ALWAYS GET NEW DATA: operations.findAllFlights()
        request.setAttribute("cities", cityMap); // OPTIONALLY ALWAYS GET NEW DATA: operations.getAllCitiesMap()

        // Forward this request to the JSP page
        request.getRequestDispatcher("/WEB-INF/pages/user.jsp").forward(request, response);

    }
}
