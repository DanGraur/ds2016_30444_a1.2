<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script src="/resources/jquery-2.2.3.min.js"></script>
<script src="/resources/jquery.lettering.js"></script>
<script src="/resources/jquery.textillate.js"></script>

<link type="text/css" rel="stylesheet" href="<c:url value="/resources/animate.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/new_style.css" />" />

<script>

    $(document).ready(function(){
        $(function () {
            $('.tlt').textillate();
        });
    });

    function getTime(id) {
        document.forms["flightForm"].hiddenId.value = id;
        document.forms["flightForm"].submit();
    }

</script>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>
            User Menu
        </title>


    </head>
    <body class = "userBody">

        <div class = "centerAlignDiv">
            <div>

                <h2 class = "tlt" data-in-effect = "fadeInUp">
                    View all flights
                </h2>

                <c:if test="${not empty flights}">
                    <form id = "flightForm" name = "flightForm" method = "post">
                        <input type="hidden" id = "hiddenId" name="hiddenId"/>

                        <table class = "w3-table-all">
                            <tr class = "w3-red">
                                <th>Flight Number</th>
                                <th>Airplane Type</th>
                                <th>Departure City</th>
                                <th>Departure Date</th>
                                <th>Departure Hour</th>
                                <th>Arrival City</th>
                                <th>Arrival Date</th>
                                <th>Arrival Hour</th>
                                <th>Departure Local Time</th>
                                <th>Arrival Local Time</th>
                            </tr>

                            <tfoot>
                            <tr>
                                <td>
                                    <label for = "logOutButton">
                                        <c:out value="Log Out: "/>
                                    </label>
                                </td>
                                <td>
                                    <input type = "submit" id = "logOutButton" name = "logOutButton" value = "Log Out" />
                                </td>

                                <!-- Add dummy elements to make the table visually pleasing START -->

                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <!-- Add dummy elements to make the table visually pleasing END -->

                            </tr>
                            </tfoot>

                            <c:forEach var="o" items="${flights}">
                                <tr>
                                    <td>
                                        <c:out value = "${o.id}"/>
                                    </td>
                                    <td>
                                        <c:out value = "${o.airplaneType}"/>
                                    </td>
                                    <td>
                                        <c:out value = "${cities[o.departureCid].name}"/>
                                    </td>
                                    <td>
                                        <c:out value = "${o.departureDate}"/>
                                    </td>
                                    <td>
                                        <c:out value = "${o.departureHour}:00"/>
                                    </td>
                                    <td>
                                        <c:out value = "${cities[o.arrivalCid].name}"/>
                                    </td>
                                    <td>
                                        <c:out value = "${o.arrivalDate}"/>
                                    </td>
                                    <td>
                                        <c:out value = "${o.arrivalHour}:00"/>
                                    </td>
                                    <td>
                                        <input class = "adminFormButton" type="submit" value="Dep. City L.T." name="departureTime" onclick="getTime(${o.departureCid})"/>
                                    </td>
                                    <td>
                                        <input class = "adminFormButton" type="submit" value="Arr. City L.T." name="arrivalTime" onclick="getTime(${o.arrivalCid})"/>
                                    </td>
                                </tr>
                            </c:forEach>

                            <c:if test = "${not empty localTime}">
                                <tr>
                                    <td>
                                        <c:out value="The local time of ${localTimeCity}:"/>
                                    </td>
                                    <td>
                                        <c:out value="${localTime}"/>
                                    </td>

                                    <!-- Add dummy elements to make the table visually pleasing START -->

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <!-- Add dummy elements to make the table visually pleasing END -->

                                </tr>
                            </c:if>
                        </table>
                    </form>
                </c:if>
            </div>
        </div>
    </body>
</html>