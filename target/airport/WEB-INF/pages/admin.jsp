<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script src="/resources/jquery-2.2.3.min.js"></script>
<script src="/resources/jquery.lettering.js"></script>
<script src="/resources/jquery.textillate.js"></script>

<link type="text/css" rel="stylesheet" href="<c:url value="/resources/animate.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/style.css" />" />
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/new_style.css" />" />

<script>

    $(document).ready(function(){
        $(function () {
            $('.tlt').textillate();
        });
    });

    function deleteFlight(id) {
        document.forms["flightForm"].hiddenId.value = id;
        document.forms["flightForm"].submit();
    }

    function updateFlight(id) {
        document.forms["flightForm"].hiddenId.value = id;
        document.forms["flightForm"].hiddenAirplaneType.value = document.getElementById("airplaneType" + id).value;
        document.forms["flightForm"].hiddenDepCid.value = document.getElementById("depCid" + id).value;
        document.forms["flightForm"].hiddenDepDate.value = document.getElementById("depDate" + id).value;
        document.forms["flightForm"].hiddenDepHour.value = document.getElementById("depHour" + id).value;
        document.forms["flightForm"].hiddenArrCid.value = document.getElementById("arrCid" + id).value;
        document.forms["flightForm"].hiddenArrDate.value = document.getElementById("arrDate" + id).value;
        document.forms["flightForm"].hiddenArrHour.value = document.getElementById("arrHour" + id).value;
        document.forms["flightForm"].submit();
    }

</script>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        Admin Menu
    </title>


</head>
<body class = "adminBody">

    <div class = "centerAlignDiv" >
        <div>

            <h2 class = "tlt" data-in-effect = "fadeInUp">
                Edit Flights
            </h2>


            <form id = "flightForm" name = "flightForm" method = "post">

                <input type="hidden" id = "hiddenId" name="hiddenId"/>
                <input type="hidden" id = "hiddenAirplaneType" name="hiddenAirplaneType"/>
                <input type="hidden" id = "hiddenDepCid" name="hiddenDepCid"/>
                <input type="hidden" id = "hiddenDepDate" name="hiddenDepDate"/>
                <input type="hidden" id = "hiddenDepHour" name="hiddenDepHour"/>
                <input type="hidden" id = "hiddenArrCid" name="hiddenArrCid"/>
                <input type="hidden" id = "hiddenArrDate" name="hiddenArrDate"/>
                <input type="hidden" id = "hiddenArrHour" name="hiddenArrHour"/>

                    <table class = "w3-table-all">
                        <tr class = "w3-red">
                            <th>Flight Number</th>
                            <th>Airplane Type</th>
                            <th>Departure City</th>
                            <th>Departure Date</th>
                            <th>Departure Hour</th>
                            <th>Arrival City</th>
                            <th>Arrival Date</th>
                            <th>Arrival Hour</th>

                            <!-- Add dummy elements to make the table visually pleasing START -->

                            <th></th>
                            <th></th>

                            <!-- Add dummy elements to make the table visually pleasing END -->

                        </tr>

                        <tfoot>
                            <tr>
                                <td>
                                    <label for = "logOutButton">
                                        <c:out value="Log Out: "/>
                                    </label>
                                </td>
                                <td>
                                    <input type = "submit" id = "logOutButton" name = "button" value = "Log Out" />
                                </td>

                                <!-- Add dummy elements to make the table visually pleasing START -->

                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <!-- Add dummy elements to make the table visually pleasing END -->

                            </tr>
                        </tfoot>

                        <tr>
                            <td>
                                <c:out value="auto-generated"/>
                            </td>
                            <td>
                                <input type = "text" name = "newFlightAirplaneType" value = "Airplane Type">
                            </td>
                            <td>
                                <select name = "newFlightDepCid">
                                    <c:forEach var="o" items="${cities}">
                                        <option value = "${o.key}">${o.value.name}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <input type = "date" name = "newFlightDepDate">
                            </td>
                            <td>
                                <select name = "newFlightDepHour">
                                    <c:forEach begin="0" end="23" varStatus="loop">
                                        <option value = "${loop.index}">${loop.index}:00</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <select name = "newFlightArrCid">
                                    <c:forEach var="o" items="${cities}">
                                        <option value = "${o.key}">${o.value.name}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <input type = "date" name = "newFlightArrDate">
                            </td>
                            <td>
                                <select name = "newFlightArrHour">
                                    <c:forEach begin="0" end="23" varStatus="loop">
                                        <option value = "${loop.index}">${loop.index}:00</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <input type = "submit" name = "button" value = "Add Flight">
                            </td>
                            <td>
                                <c:out value = "${statusMessage}" />
                            </td>
                        </tr>
                        <c:if test="${not empty flights}">
                            <c:forEach var="o" items="${flights}">
                                <tr>
                                    <td>
                                        <c:out value = "${o.id}"/>
                                    </td>
                                    <td>
                                        <input class = "myInput" type = "text" size="10" readonly="readonly" id="airplaneType${o.id}" ondblclick="this.readOnly='';" value = "${o.airplaneType}"/>
                                    </td>
                                    <td>
                                        <select id = "depCid${o.id}" >
                                            <c:forEach var="c" items="${cities}">
                                                <option value = "${c.key}" <c:if test = "${o.departureCid == c.key}"> selected </c:if>>${c.value.name}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <input type = "date" id = "depDate${o.id}" readonly="readonly" ondblclick="this.readOnly='';" value = "${datePairs[o.id].departureDate}">
                                    </td>
                                    <td>
                                        <select id = "depHour${o.id}">
                                            <c:forEach begin="0" end="23" varStatus="loop">
                                                <option value = "${loop.index}" <c:if test = "${o.departureHour == loop.index}"> selected </c:if>>${loop.index}:00</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <select id = "arrCid${o.id}">
                                            <c:forEach var="c" items="${cities}">
                                                <option value = "${c.key}" <c:if test = "${o.arrivalCid == c.key}"> selected </c:if>>${c.value.name}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <input type = "date" id = "arrDate${o.id}" readonly="readonly" ondblclick="this.readOnly='';" value = "${datePairs[o.id].arrivalDate}">
                                    </td>
                                    <td>
                                        <select id = "arrHour${o.id}" >
                                            <c:forEach begin="0" end="23" varStatus="loop">
                                                <option value = "${loop.index}" <c:if test = "${o.arrivalHour == loop.index}"> selected </c:if>>${loop.index}:00</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="submit" value="Update Flight" name="button" onclick="updateFlight(${o.id})"/>
                                    </td>
                                    <td>
                                        <input type="submit" value="Delete Flight" name="button" onclick="deleteFlight(${o.id})"/>
                                    </td>
                                </tr>
                            </c:forEach>
                         </c:if>
                    </table>
            </form>
        </div>
    </div>
</body>
</html>